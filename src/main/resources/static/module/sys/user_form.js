layui.use(['layer', 'admin', 'form', 'formSelects'], function () {
        var layer = layui.layer;
        var admin = layui.admin;
        var form = layui.form;
        var formSelects = layui.formSelects;

        form.render('radio');

        // 获取所有角色
        layer.load(2);
        admin.req('sys/role/all', {}, function (data) {
            layer.closeAll('loading');
            if (0 == data.code) {
                // 渲染多选下拉框
                var roleSelectData = new Array();
                for (var i = 0; i < data.data.length; i++) {
                    roleSelectData.push({name: data.data[i].roleName, value: data.data[i].id});
                }
                formSelects.data('roleId', 'local', {arr: roleSelectData});

                // 回显user数据
                var user = admin.getTempData('t_user');
                $('#user-form').attr('method', 'POST');
                $('#user-form').attr('da', 'add');
                if (user) {
                    form.val('user-form', user);
                    $('#user-form').attr('da', 'edit');
                    var rds = new Array();
                    for (var i = 0; i < user.roles.length; i++) {
                        rds.push(user.roles[i].id);
                    }
                    formSelects.value('roleId', rds);
                }

            } else {
                layer.msg('获取角色失败', {icon: 2});
            }
        }, 'GET');

        // 表单提交事件
        form.on('submit(user-form-submit)', function (data) {
            layer.load(2);
            admin.req('sys/user/'+$('#user-form').attr("da"), data.field, function (data) {
                layer.closeAll('loading');
                if (data.code == 200) {
                    layer.msg(data.msg, {icon: 1});
                    admin.finishPopupCenter();
                } else {
                    layer.msg(data.msg, {icon: 2});
                }
            }, $('#user-form').attr('method'));
            return false;
        });
    });