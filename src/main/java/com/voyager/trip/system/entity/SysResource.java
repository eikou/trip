package com.voyager.trip.system.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

@TableName("sys_resource")
public class SysResource implements Serializable {

	private static final long serialVersionUID = -4729811794152390151L;

	@TableId
	private String id;

	private String parentId;

	private String resName;

	private String resUrl;

	private String resMethod;

	private Integer resWeight;

	private Integer resType;

	private String resIcon;

	private String authority;

	private String authorityDesc;

	private Integer isLeaf;

	private String createUser;

	private Date createTime;

	private String updateUser;

	private Date updateTime;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}

	public String getResName() {
		return resName;
	}

	public void setResName(String resName) {
		this.resName = resName;
	}

	public String getResUrl() {
		return resUrl;
	}

	public void setResUrl(String resUrl) {
		this.resUrl = resUrl;
	}

	public String getResMethod() {
		return resMethod;
	}

	public void setResMethod(String resMethod) {
		this.resMethod = resMethod;
	}

	public Integer getResWeight() {
		return resWeight;
	}

	public void setResWeight(Integer resWeight) {
		this.resWeight = resWeight;
	}

	public Integer getResType() {
		return resType;
	}

	public void setResType(Integer resType) {
		this.resType = resType;
	}

	public String getResIcon() {
		return resIcon;
	}

	public void setResIcon(String resIcon) {
		this.resIcon = resIcon;
	}

	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	public String getAuthorityDesc() {
		return authorityDesc;
	}

	public void setAuthorityDesc(String authorityDesc) {
		this.authorityDesc = authorityDesc;
	}

	public Integer getIsLeaf() {
		return isLeaf;
	}

	public void setIsLeaf(Integer isLeaf) {
		this.isLeaf = isLeaf;
	}

	public String getCreateUser() {
		return createUser;
	}

	public void setCreateUser(String createUser) {
		this.createUser = createUser;
	}

	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	public String getUpdateUser() {
		return updateUser;
	}

	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser;
	}

	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "SysResource [id=" + id + ", parentId=" + parentId + ", resName=" + resName + ", resUrl=" + resUrl
				+ ", resMethod=" + resMethod + ", resWeight=" + resWeight + ", resType=" + resType + ", resIcon="
				+ resIcon + ", authority=" + authority + ", authorityDesc=" + authorityDesc + ", isLeaf=" + isLeaf
				+ ", createUser=" + createUser + ", createTime=" + createTime + ", updateUser=" + updateUser
				+ ", updateTime=" + updateTime + "]";
	}

}