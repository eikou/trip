package com.voyager.trip.system.entity;

import org.springframework.security.core.GrantedAuthority;

public class MyGrantedAuthority implements GrantedAuthority {

	private static final long serialVersionUID = -6006624240698299887L;
	
	private String url;
	private String method;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}
	
	@Override
	public String getAuthority() {
		return this.method + ":" +this.url;
	}
}
