package com.voyager.trip.system.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.voyager.trip.common.result.DicContent;
import com.voyager.trip.common.result.JsonResult;
import com.voyager.trip.common.result.PageResult;
import com.voyager.trip.common.utils.UUIDUtil;
import com.voyager.trip.system.dao.SysRoleMapper;
import com.voyager.trip.system.entity.SysRole;
import com.voyager.trip.system.service.RoleService;

@Service
public class RoleServiceImpl implements RoleService {

	@Autowired
	private SysRoleMapper sysRoleMapper;
	
	@Override
	public List<SysRole> getAll() {
		Wrapper<SysRole> wrapper = new QueryWrapper<SysRole>().orderByDesc("create_time");
		return sysRoleMapper.selectList(wrapper);
	}
	
	@Override
	public PageResult<SysRole> getPageList(Integer pageNo, Integer pageSize, SysRole sysRole) {
		PageResult<SysRole> pageResult = new PageResult<SysRole>();
		Page<SysRole> page = new Page<SysRole>(pageNo, pageSize);
		Wrapper<SysRole> wrapper = new QueryWrapper<SysRole>().orderByDesc("create_time");
		IPage<SysRole> iPage = sysRoleMapper.selectPage(page, wrapper);
		pageResult.setCode(DicContent.OK);
		pageResult.setCount(iPage.getTotal());
		pageResult.setData(iPage.getRecords());
		return pageResult;
	}

	@Override
	public JsonResult addRole(SysRole sysRole) {
		sysRole.setId(UUIDUtil.randomUUID32());
		sysRole.setCreateTime(new Date());
		sysRole.setUpdateTime(new Date());
		if (sysRoleMapper.insert(sysRole) == 1) {
			return JsonResult.ok("add role success");
		} else {
			return JsonResult.error("add role fail");
		}
	}

	@Override
	public JsonResult editRole(SysRole sysRole) {
		sysRole.setUpdateTime(new Date());
		if (sysRoleMapper.updateById(sysRole) == 1) {
			return JsonResult.ok("edit role success");
		} else {
			return JsonResult.error("edit role fail");
		}
	}

	@Override
	public JsonResult delRoles(List<String> idList) {
		if (sysRoleMapper.deleteBatchIds(idList) > 0) {
			return JsonResult.ok("delete role success");
		} else {
			return JsonResult.error("delete role fail");
		}
	}

	@Override
	public JsonResult getRoleById(String id) {
		SysRole sysRole = sysRoleMapper.selectById(id);
		return JsonResult.ok("get role success").put("role", sysRole);
	}

}
