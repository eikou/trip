package com.voyager.trip.system.service;

import com.voyager.trip.common.result.JsonResult;
import com.voyager.trip.common.result.PageResult;
import com.voyager.trip.system.entity.SysUser;

public interface UserService {

	SysUser getUserById(String id);

	PageResult<SysUser> getPageList(Integer pageNo, Integer pageSize, String column, String value);
	
	JsonResult addUser(SysUser sysUser);
	
	JsonResult editUser(SysUser sysUser);
	
	JsonResult delUser(String id);
	
	JsonResult modifyPassword(String id, String newPassword);
	
	JsonResult resetPassword(String id);
}
