package com.voyager.trip.system.service;

import java.util.List;

import com.voyager.trip.common.result.JsonResult;
import com.voyager.trip.common.result.PageResult;
import com.voyager.trip.system.entity.SysRole;

public interface RoleService {

	PageResult<SysRole> getPageList(Integer pageNo, Integer pageSize, SysRole sysRole);
	
	JsonResult addRole(SysRole sysRole);
	
	JsonResult editRole(SysRole sysRole);
	
	JsonResult delRoles(List<String> idList);
	
	JsonResult getRoleById(String id);

	List<SysRole> getAll();
}
