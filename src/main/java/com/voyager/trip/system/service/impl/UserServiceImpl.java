package com.voyager.trip.system.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.voyager.trip.common.result.JsonResult;
import com.voyager.trip.common.result.PageResult;
import com.voyager.trip.common.utils.UUIDUtil;
import com.voyager.trip.system.dao.SysUserMapper;
import com.voyager.trip.system.entity.SysUser;
import com.voyager.trip.system.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private SysUserMapper sysUserMapper;
	
	@Override
	public SysUser getUserById(String id) {
		return sysUserMapper.selectById(id);
	}

	@Override
	public PageResult<SysUser> getPageList(Integer pageNo, Integer pageSize, String column, String value) {
		Page<SysUser> page = new Page<SysUser>(pageNo, pageSize);
//		Wrapper<SysUser> wrapper = new QueryWrapper<SysUser>(sysUser).orderByDesc("create_time");
		Wrapper<SysUser> wrapper = null;
		if (StringUtils.hasLength(column)) {
			wrapper = new QueryWrapper<SysUser>().like(column, value).orderByDesc("create_time");
		} else {
			wrapper = new QueryWrapper<SysUser>().orderByDesc("create_time");
		}
		IPage<SysUser> iPage = sysUserMapper.selectPage(page, wrapper);
		iPage.getRecords().forEach(u -> u.setPassword(null));
		return new PageResult<SysUser>(iPage.getTotal(), iPage.getRecords());
	}

	@Override
	public JsonResult addUser(SysUser sysUser) {
		String finalSecret = "{bcrypt}" + new BCryptPasswordEncoder().encode((sysUser.getPassword()!=null ? sysUser.getPassword() : "123456"));
		sysUser.setId(UUIDUtil.randomUUID32());
		sysUser.setPassword(finalSecret);
		sysUser.setStatus(1);
		sysUser.setCreateTime(new Date());
		sysUser.setUpdateTime(new Date());
		if (sysUserMapper.insert(sysUser) == 1) {
			return JsonResult.ok("添加成功");
		} else {
			return JsonResult.error("添加失败");
		}
	}

	@Override
	public JsonResult editUser(SysUser sysUser) {
		if (sysUser!=null && !StringUtils.isEmpty(sysUser.getPassword())) {
			sysUser.setPassword("{bcrypt}" + new BCryptPasswordEncoder().encode(sysUser.getPassword()));
		}
		sysUser.setUpdateTime(new Date());
		if (sysUserMapper.updateById(sysUser) == 1) {
			return JsonResult.ok("修改成功");
		} else {
			return JsonResult.error("修改失败");
		}
	}

	@Override
	public JsonResult delUser(String id) {
		if (sysUserMapper.deleteById(id) == 1) {
			return JsonResult.ok("删除成功");
		} else {
			return JsonResult.error("删除失败");
		}
	}

	@Override
	public JsonResult modifyPassword(String id, String newPassword) {
		SysUser sysUser = new SysUser();
		sysUser.setId(id);
		sysUser.setPassword("{bcrypt}" + new BCryptPasswordEncoder().encode(newPassword));
		sysUser.setUpdateTime(new Date());
		if (sysUserMapper.updateById(sysUser) == 1) {
			return JsonResult.ok("密码已修改");
		} else {
			return JsonResult.error("密码修改失败");
		}
	}

	@Override
	public JsonResult resetPassword(String id) {
		SysUser sysUser = new SysUser();
		sysUser.setId(id);
		sysUser.setPassword("{bcrypt}" + new BCryptPasswordEncoder().encode("123456"));
		sysUser.setUpdateTime(new Date());
		if (sysUserMapper.updateById(sysUser) == 1) {
			return JsonResult.ok("密码已重置");
		} else {
			return JsonResult.error("密码重置失败");
		}
	}
	
	

}
