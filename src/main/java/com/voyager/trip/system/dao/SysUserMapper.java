package com.voyager.trip.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.voyager.trip.system.entity.SysUser;

public interface SysUserMapper extends BaseMapper<SysUser> {
    SysUser getUserByAccount(String account);
    
    /**
     * 用户分页查询
     * @param page 分页对象,xml中可以从里面进行取值,传递参数 Page 即自动分页,必须放在第一位(你可以继承Page实现自己的分页对象)
     * @param sysUser
     * @return
     */
    IPage<SysUser> selectPageList(Page<SysUser> page, SysUser sysUser);
}