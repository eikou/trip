package com.voyager.trip.system.dao;

import java.util.List;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.voyager.trip.system.entity.SysResource;

public interface SysResourceMapper extends BaseMapper<SysResource> {
	List<SysResource> getResourcesByUserId(String userId);

	List<SysResource> findAll();
}