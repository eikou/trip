package com.voyager.trip.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.voyager.trip.system.entity.SysRoleResourceKey;

public interface SysRoleResourceMapper extends BaseMapper<SysRoleResourceKey> {
	
}