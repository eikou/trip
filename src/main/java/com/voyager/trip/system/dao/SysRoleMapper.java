package com.voyager.trip.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.voyager.trip.system.entity.SysRole;

public interface SysRoleMapper extends BaseMapper<SysRole> {

}