package com.voyager.trip.system.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.voyager.trip.common.result.JsonResult;
import com.voyager.trip.common.web.BaseController;
import com.voyager.trip.system.entity.SysUser;

@RestController
public class MainController extends BaseController {

	
	@GetMapping("/userInfo")
    public JsonResult userInfo() {
		SysUser curUser = getLoginUser();
		curUser.setPassword("");
        return JsonResult.ok().put("user", curUser);
    }
}
