package com.voyager.trip.file.dao;

import com.voyager.trip.file.entity.ResFile;

public interface ResFileMapper {
    int deleteByPrimaryKey(String id);

    int insert(ResFile record);

    int insertSelective(ResFile record);

    ResFile selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ResFile record);

    int updateByPrimaryKey(ResFile record);
}