package com.voyager.trip.file.controller;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.voyager.trip.common.config.UploadConfig;
import com.voyager.trip.common.result.JsonResult;
import com.voyager.trip.common.utils.UUIDUtil;
import com.voyager.trip.common.utils.Validator;
import com.voyager.trip.common.web.BaseController;
import com.voyager.trip.file.entity.ResFile;
import com.voyager.trip.file.service.ResFileService;

@RestController
@RequestMapping("/upload")
public class FileUploadController extends BaseController {

	@Autowired
	private UploadConfig uploadConfig;
	@Autowired
	private ResFileService resFileService;
	
	@CrossOrigin
	@PostMapping("/image")
	public JsonResult uploadImage(@RequestParam("file") MultipartFile[] file, HttpServletRequest request) {
		List<String> fileUrls = new ArrayList<String>();
		if (null != file && file.length > 0) {
			// 遍历并保存文件
	        for (MultipartFile files : file) {
	        	// 取得当前上传文件的文件名称
	            String fileName = files.getOriginalFilename();
	            // 验证格式
	            if (!Validator.isImage(fileName)) {
	            	return JsonResult.error("上传文件必须是图片");
	            }
	            // 新文件名
	            String fileNameNew = UUIDUtil.randomUUID32() + fileName.substring(fileName.lastIndexOf("."));
	            //创建文件
	            String filePath = File.separator + "files" + File.separator + "image" + File.separator + new SimpleDateFormat("yyyy-MM-dd").format(new Date()) + File.separator + fileNameNew;
	            File dest = new File(uploadConfig.getUploadPath() + filePath);
	            if(!dest.getParentFile().exists()){ //判断文件父目录是否存在
	                dest.getParentFile().mkdir();
	            }
	            try {
					files.transferTo(dest);
					fileUrls.add(uploadConfig.getUrlprefix() + filePath);
					ResFile resFile = new ResFile();
					resFile.setCreateUser(super.getLoginUserAccount());
					resFile.setCreateTime(new Date());
					resFile.setUpdateTime(new Date());
					resFile.setUpdateUser(super.getLoginUserAccount());
					resFile.setId(UUIDUtil.randomUUID32());
					resFile.setFileName(fileNameNew);
					resFile.setOriginalName(fileName);
					resFile.setPath(uploadConfig.getUploadPath() + filePath);
					resFile.setUrl(uploadConfig.getUrlprefix() + filePath);
					resFileService.insert(resFile);
				} catch (IllegalStateException e) {
					e.printStackTrace();
					return JsonResult.error("上传文件失败");
				} catch (IOException e) {
					e.printStackTrace();
					return JsonResult.error("上传文件失败");
				}
	        }
			
		} else {
			return JsonResult.error("上传文件不能为空");
		}
		return JsonResult.ok().put("fileUrls", fileUrls);
	}
	
}
