package com.voyager.trip.file.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.voyager.trip.file.dao.ResFileMapper;
import com.voyager.trip.file.entity.ResFile;
import com.voyager.trip.file.service.ResFileService;

@Service
public class ResFileServiceImpl implements ResFileService {

	@Autowired
	private ResFileMapper resFileMapper;
	
	@Override
	public int insert(ResFile resFile) {
		return resFileMapper.insert(resFile);
	}

}
