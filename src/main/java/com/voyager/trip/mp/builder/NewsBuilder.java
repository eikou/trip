package com.voyager.trip.mp.builder;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutNewsMessage;

import java.util.List;

public class NewsBuilder extends AbstractBuilder<List<WxMpXmlOutNewsMessage.Item>>  {

    public WxMpXmlOutNewsMessage build(List<WxMpXmlOutNewsMessage.Item> articles, WxMpXmlMessage wxMessage,
                                   WxMpService service) {
        WxMpXmlOutNewsMessage m = WxMpXmlOutMessage.NEWS().articles(articles)
                .fromUser(wxMessage.getToUser()).toUser(wxMessage.getFromUser())
                .build();
        return m;
    }
}
