package com.voyager.trip.mp.model;

import java.io.Serializable;

/**
 * 功能说明: 公众号<br>
 * 系统说明: <br>
 * 模块说明: <br>
 * 功能描述: <br>
 * <br>
 */
public class AccountBO implements Serializable {

    private String publicKey;
    private String appId;
    private String appSecret;
    private String appToken;
    private String accessTokenHandler;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getAppSecret() {
        return appSecret;
    }

    public void setAppSecret(String appSecret) {
        this.appSecret = appSecret;
    }

    public String getAppToken() {
        return appToken;
    }

    public void setAppToken(String appToken) {
        this.appToken = appToken;
    }

    public String getAccessTokenHandler() {
        return accessTokenHandler;
    }

    public void setAccessTokenHandler(String accessTokenHandler) {
        this.accessTokenHandler = accessTokenHandler;
    }
}
