package com.voyager.trip.mp.service;

import com.voyager.trip.mp.model.MpMenuBO;

import java.util.List;

public interface MpMenuService {
    List<MpMenuBO> getMenus();

    MpMenuBO getMpMenuById(String menuId);
}
