package com.voyager.trip.mp.service.impl;

import com.voyager.trip.mp.dao.MpMenuDao;
import com.voyager.trip.mp.domain.MpMenuPO;
import com.voyager.trip.mp.model.MpMenuBO;
import com.voyager.trip.mp.service.MpMenuService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class MpMenuServiceImpl implements MpMenuService {

    final Logger LOGGER = LoggerFactory.getLogger(this.getClass());

    @Autowired
    MpMenuDao mpMenuDao;


    @Override
    public List<MpMenuBO> getMenus() {
        List<MpMenuBO> menuList = new ArrayList<>();

        mpMenuDao.queryAll()
                .stream()
                .filter(MpMenuPO::getEnabled)
                .forEach(po -> {
                    MpMenuBO bo = new MpMenuBO();
                    BeanUtils.copyProperties(po, bo);
                    menuList.add(bo);
                });

        return menuList;
    }

    @Override
    public MpMenuBO getMpMenuById(String menuId) {

        MpMenuPO po = mpMenuDao.getMpMenuById(menuId);
        if (!Objects.isNull(po)) {
            MpMenuBO bo = new MpMenuBO();
            BeanUtils.copyProperties(bo, po);
            return bo;
        }
        return null;
    }
}
