package com.voyager.trip.mp.common.dict;

/**
 * 功能说明: <br>
 * 系统说明: <br>
 * 模块说明: <br>
 * 功能描述: <br>
 * <br>
 */
public class RedisKey {

    public final static String TOKEN_PREFIX = "mp.account.token";
    public final static String ACCOUNTS_PREFIX = "mp.account.list";
    public final static String USER_TASK_QUEUE = "mp.user.queue.task";

    public final static String ACCOUNT_MENUS = "mp.account.menus";
}
