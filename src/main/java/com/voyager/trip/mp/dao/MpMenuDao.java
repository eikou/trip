package com.voyager.trip.mp.dao;

import com.voyager.trip.mp.domain.MpMenuPO;

import java.util.List;

public interface MpMenuDao {

    List<MpMenuPO> queryAll();

    MpMenuPO getMpMenuById(String menuId);
}
