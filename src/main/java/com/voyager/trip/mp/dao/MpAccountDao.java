package com.voyager.trip.mp.dao;

import com.voyager.trip.mp.domain.MpAccountPO;

import java.util.List;

/**
 * 功能说明: 微信号<br>
 * 系统说明: <br>
 * 模块说明: <br>
 * 功能描述: <br>
 * <br>
 */
public interface MpAccountDao {

    List<MpAccountPO> queryAll();
}
