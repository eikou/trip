package com.voyager.trip.mp.dao.impl;

import com.voyager.trip.mp.dao.MpAccountDao;
import com.voyager.trip.mp.domain.MpAccountPO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 功能说明: <br>
 * 系统说明: <br>
 * 模块说明: <br>
 * 功能描述: <br>
 * <br>
 */
@Repository
public class MpAccountDaoJdbc implements MpAccountDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public List<MpAccountPO> queryAll() {
        return jdbcTemplate.query(Q_ALL, (rs, i) -> {
            MpAccountPO po = new MpAccountPO();
            po.setId(rs.getString("ID"));
            po.setPublicKey(rs.getString("PUBLIC_KEY"));
            po.setAppId(rs.getString("APP_ID"));
            po.setAppSecret(rs.getString("APP_SECRET"));
            po.setAppToken(rs.getString("token"));
            po.setAccessTokenHandler(rs.getString("TOKEN_HANDLER"));
            po.setEnabled(rs.getBoolean("ENABLED"));
            return po;
        });
    }

    final static String Q_ALL = "select id, public_key, app_id, app_secret, token, token_handler, enabled from wechat_account";
}
