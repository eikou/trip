package com.voyager.trip.mp.datastore.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.voyager.trip.mp.datastore.ShareStore;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

/**
 * 功能说明: <br>
 * 系统说明: <br>
 * 模块说明: <br>
 * 功能描述: <br>
 * <br>
 */
@Component
public class ShareStoreImpl implements ShareStore {

    @Autowired
    StringRedisTemplate redisTemplate;

    ObjectMapper objectMapper = new ObjectMapper();

    @Override
    @Cacheable(value = "share_store", key = "#key1 + '.' + #key2")
    public <T> T getByKey(String key1, String key2, Class<T> tClass) throws Exception {
        String val = redisTemplate.<String, String>opsForHash().get(key1, key2);
        return objectMapper.readValue(val, tClass);
    }
}
