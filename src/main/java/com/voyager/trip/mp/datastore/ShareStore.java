package com.voyager.trip.mp.datastore;

/**
 * 功能说明: <br>
 * 系统说明: <br>
 * 模块说明: <br>
 * 功能描述: <br>
 * <br>
 */
public interface ShareStore {

    <T> T getByKey(String key1, String key2, Class<T> tClass) throws Exception;
}
