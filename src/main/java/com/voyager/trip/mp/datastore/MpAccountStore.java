package com.voyager.trip.mp.datastore;

import com.voyager.trip.mp.common.dict.RedisKey;
import com.voyager.trip.mp.model.AccountBO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

/**
 * 功能说明: <br>
 * 系统说明: <br>
 * 模块说明: <br>
 * 功能描述: <br>
 * <br>
 */
@Component
public class MpAccountStore {

    @Autowired
    ShareStore shareStore;

    public Optional<AccountBO> get(String publicKey) {
        try {
            AccountBO bo = shareStore.getByKey(RedisKey.ACCOUNTS_PREFIX, publicKey, AccountBO.class);
            return Optional.of(bo);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
