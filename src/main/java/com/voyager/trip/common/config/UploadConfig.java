package com.voyager.trip.common.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "trip.file")
public class UploadConfig {

	private String uploadPath; // 上传路径
	private String urlprefix; // url前缀，域名

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}

	public String getUrlprefix() {
		return urlprefix;
	}

	public void setUrlprefix(String urlprefix) {
		this.urlprefix = urlprefix;
	}

}
