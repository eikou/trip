package com.voyager.trip.common.config;

import java.io.File;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Component
public class WebConfigurer extends WebMvcConfigurationSupport {

	@Autowired
	UploadConfig uploadConfig;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/files/**").addResourceLocations("file:///"+uploadConfig.getUploadPath()+ File.separator + "files" + File.separator + "image");
	}
	
}
