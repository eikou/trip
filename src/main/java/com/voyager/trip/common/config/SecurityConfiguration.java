package com.voyager.trip.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.intercept.FilterSecurityInterceptor;

import com.voyager.trip.system.service.impl.MyFilterSecurityInterceptor;
import com.voyager.trip.system.service.impl.UserDetailsServiceImpl;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
    private MyFilterSecurityInterceptor myFilterSecurityInterceptor;
	
	@Bean
	@Override
	protected UserDetailsService userDetailsService() {
		return new UserDetailsServiceImpl();
	}

	/**
	 * password加密方案，通过密码的前缀区分编码方式，支持多种编码
	 */
	@Bean
	public PasswordEncoder passwordEncoder() {
		return PasswordEncoderFactories.createDelegatingPasswordEncoder();
	}

	/**
	 * SpringBoot2.0需要暴露authenticationManager
	 */
	@Bean
	@Override
	public AuthenticationManager authenticationManagerBean() throws Exception {
		return super.authenticationManagerBean();
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		// 对oauth开头的请求不拦截
		http.authorizeRequests()
				.antMatchers("/oauth/*").permitAll()
//				.antMatchers("/upload/**").permitAll()
				.antMatchers("/api/**").permitAll() // h5前端接口
				.and()
				.addFilterBefore(myFilterSecurityInterceptor, FilterSecurityInterceptor.class)
		.csrf().disable()
		;
	}

}
