package com.voyager.trip.common.utils;

public class Validator {

	private static final String FILE_IMAGE = ".jpg|.jpeg|.png|.bmp";
	
	
	public static boolean isImage(String fileName) {
		if (fileName != null) {
			return FILE_IMAGE.contains(fileName.substring(fileName.lastIndexOf(".")).toLowerCase());
		} else {
			return false;
		}
	}
	
	public static void main(String[] args) {
		System.out.println(isImage("sdfsd.jPg"));
	}
}
