package com.voyager.trip.common.exception;

/**
 * 自定义异常类
 */
public abstract class IException extends RuntimeException {

	private static final long serialVersionUID = -327596858916527556L;

	private Integer code;
	
	public IException() {
    }

    public IException(String message) {
        super(message);
    }

    public IException(Integer code, String message) {
        super(message);
        this.code = code;
    }

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}
