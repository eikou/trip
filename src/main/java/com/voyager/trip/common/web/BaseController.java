package com.voyager.trip.common.web;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.voyager.trip.common.exception.BusinessException;
import com.voyager.trip.common.result.DicContent;
import com.voyager.trip.system.entity.SysUser;

/**
 * Controller基类
 */
public class BaseController {

    /**
     * 获取当前登录的user
     */
    public SysUser getLoginUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            Object object = authentication.getPrincipal();
            if (object != null && !object.equals("anonymousUser")) {
                return (SysUser) object;
            } else {
            	throw new BusinessException(DicContent.NOT_LOGIN, "not login");
            }
        } else {
        	throw new BusinessException(DicContent.NOT_LOGIN, "not login");
        }
//        return null;
    }
    
    /**
	   * 获取当前登录用户账号
     * @return
     */
    public String getLoginUserAccount() {
    	SysUser loginUser = getLoginUser();
        return loginUser == null ? null : loginUser.getAccount();
    }

    /**
     * 获取当前登录的userId
     */
    public String getLoginUserId() {
    	SysUser loginUser = getLoginUser();
        return loginUser == null ? null : loginUser.getId();
    }

}
