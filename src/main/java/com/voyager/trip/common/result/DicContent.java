package com.voyager.trip.common.result;

public class DicContent {

	public static final Integer OK = 200; // 成功
	public static final Integer ERROR = 500; // 失败
	public static final Integer NOT_LOGIN = 1000; // 用户未登录
	
	public static final Integer INVALID_ACCESS_TOKEN = 8000; // access token 无效
	public static final Integer NO_RIGHTS = 8001;  // 没有权限
	public static final Integer AUTHENTICATION_FAILURE = 8002; // 认证失败
}
