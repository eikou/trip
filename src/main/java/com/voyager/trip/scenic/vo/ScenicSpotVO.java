package com.voyager.trip.scenic.vo;

import com.voyager.trip.scenic.entity.ScenicMap;

public class ScenicSpotVO {

	private String id;

	private String name;

	private String nameJp;

	private String summary;

	private String summaryJp;

	private String description;

	private String descriptionJp;

	private String imageUrl;

	private String openTimes;

	private String rideGuide;

	private String bestTravelTime;

	private Integer orderSeq;

	private String graphicAppreciation;

	private ScenicMap scenicMap;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameJp() {
		return nameJp;
	}

	public void setNameJp(String nameJp) {
		this.nameJp = nameJp;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getSummaryJp() {
		return summaryJp;
	}

	public void setSummaryJp(String summaryJp) {
		this.summaryJp = summaryJp;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionJp() {
		return descriptionJp;
	}

	public void setDescriptionJp(String descriptionJp) {
		this.descriptionJp = descriptionJp;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getOpenTimes() {
		return openTimes;
	}

	public void setOpenTimes(String openTimes) {
		this.openTimes = openTimes;
	}

	public String getRideGuide() {
		return rideGuide;
	}

	public void setRideGuide(String rideGuide) {
		this.rideGuide = rideGuide;
	}

	public String getBestTravelTime() {
		return bestTravelTime;
	}

	public void setBestTravelTime(String bestTravelTime) {
		this.bestTravelTime = bestTravelTime;
	}

	public Integer getOrderSeq() {
		return orderSeq;
	}

	public void setOrderSeq(Integer orderSeq) {
		this.orderSeq = orderSeq;
	}

	public String getGraphicAppreciation() {
		return graphicAppreciation;
	}

	public void setGraphicAppreciation(String graphicAppreciation) {
		this.graphicAppreciation = graphicAppreciation;
	}

	public ScenicMap getScenicMap() {
		return scenicMap;
	}

	public void setScenicMap(ScenicMap scenicMap) {
		this.scenicMap = scenicMap;
	}

	@Override
	public String toString() {
		return "ScenicSpotVO [id=" + id + ", name=" + name + ", nameJp=" + nameJp + ", summary=" + summary
				+ ", summaryJp=" + summaryJp + ", description=" + description + ", descriptionJp=" + descriptionJp
				+ ", imageUrl=" + imageUrl + ", openTimes=" + openTimes + ", rideGuide=" + rideGuide
				+ ", bestTravelTime=" + bestTravelTime + ", orderSeq=" + orderSeq + ", graphicAppreciation="
				+ graphicAppreciation + ", scenicMap=" + scenicMap + "]";
	}

}
