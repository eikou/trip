package com.voyager.trip.scenic.entity;

import java.util.Date;

public class ScenicRouteDetail {
    private String id;

    private String scenicRouteId;

    private String scenicSpotId;

    private Integer scenicSpotSeq;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getScenicRouteId() {
        return scenicRouteId;
    }

    public void setScenicRouteId(String scenicRouteId) {
        this.scenicRouteId = scenicRouteId;
    }

    public String getScenicSpotId() {
        return scenicSpotId;
    }

    public void setScenicSpotId(String scenicSpotId) {
        this.scenicSpotId = scenicSpotId;
    }

    public Integer getScenicSpotSeq() {
        return scenicSpotSeq;
    }

    public void setScenicSpotSeq(Integer scenicSpotSeq) {
        this.scenicSpotSeq = scenicSpotSeq;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", scenicRouteId=").append(scenicRouteId);
        sb.append(", scenicSpotId=").append(scenicSpotId);
        sb.append(", scenicSpotSeq=").append(scenicSpotSeq);
        sb.append(", createUser=").append(createUser);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateUser=").append(updateUser);
        sb.append(", updateTime=").append(updateTime);
        sb.append("]");
        return sb.toString();
    }
}