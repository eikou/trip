package com.voyager.trip.scenic.entity;

import java.util.Date;

public class ScenicSpot {
    private String id;

    private String name;

    private String nameJp;

    private String summary;

    private String summaryJp;

    private String description;

    private String descriptionJp;

    private String imageUrl;

    private String openTimes;

    private String rideGuide;

    private String bestTravelTime;

    private Integer orderSeq;

    private String createUser;

    private Date createTime;

    private String updateUser;

    private Date updateTime;

    private String graphicAppreciation;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameJp() {
        return nameJp;
    }

    public void setNameJp(String nameJp) {
        this.nameJp = nameJp;
    }

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getSummaryJp() {
        return summaryJp;
    }

    public void setSummaryJp(String summaryJp) {
        this.summaryJp = summaryJp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescriptionJp() {
        return descriptionJp;
    }

    public void setDescriptionJp(String descriptionJp) {
        this.descriptionJp = descriptionJp;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getOpenTimes() {
        return openTimes;
    }

    public void setOpenTimes(String openTimes) {
        this.openTimes = openTimes;
    }

    public String getRideGuide() {
        return rideGuide;
    }

    public void setRideGuide(String rideGuide) {
        this.rideGuide = rideGuide;
    }

    public String getBestTravelTime() {
        return bestTravelTime;
    }

    public void setBestTravelTime(String bestTravelTime) {
        this.bestTravelTime = bestTravelTime;
    }

    public Integer getOrderSeq() {
        return orderSeq;
    }

    public void setOrderSeq(Integer orderSeq) {
        this.orderSeq = orderSeq;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getGraphicAppreciation() {
        return graphicAppreciation;
    }

    public void setGraphicAppreciation(String graphicAppreciation) {
        this.graphicAppreciation = graphicAppreciation;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", nameJp=").append(nameJp);
        sb.append(", summary=").append(summary);
        sb.append(", summaryJp=").append(summaryJp);
        sb.append(", description=").append(description);
        sb.append(", descriptionJp=").append(descriptionJp);
        sb.append(", imageUrl=").append(imageUrl);
        sb.append(", openTimes=").append(openTimes);
        sb.append(", rideGuide=").append(rideGuide);
        sb.append(", bestTravelTime=").append(bestTravelTime);
        sb.append(", orderSeq=").append(orderSeq);
        sb.append(", createUser=").append(createUser);
        sb.append(", createTime=").append(createTime);
        sb.append(", updateUser=").append(updateUser);
        sb.append(", updateTime=").append(updateTime);
        sb.append(", graphicAppreciation=").append(graphicAppreciation);
        sb.append("]");
        return sb.toString();
    }
}