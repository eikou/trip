package com.voyager.trip.scenic.dao;

import java.util.List;

import com.voyager.trip.scenic.vo.ScenicSpotVO;

public interface ScenicSpotWithMapMapper {
    
    List<ScenicSpotVO> selectScenicListByGroupType(Integer groupType);
    
    List<ScenicSpotVO> selectScenicAllList();
    
    ScenicSpotVO selectScenicSpotAndMapById(String id);
}