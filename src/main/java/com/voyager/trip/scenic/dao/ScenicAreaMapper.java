package com.voyager.trip.scenic.dao;

import com.voyager.trip.scenic.entity.ScenicArea;

public interface ScenicAreaMapper {
    int deleteByPrimaryKey(String id);

    int insert(ScenicArea record);

    int insertSelective(ScenicArea record);

    ScenicArea selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ScenicArea record);

    int updateByPrimaryKey(ScenicArea record);
}