package com.voyager.trip.scenic.dao;

import com.voyager.trip.scenic.entity.ScenicSpotGroup;

public interface ScenicSpotGroupMapper {
    int deleteByPrimaryKey(String id);

    int insert(ScenicSpotGroup record);

    int insertSelective(ScenicSpotGroup record);

    ScenicSpotGroup selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ScenicSpotGroup record);

    int updateByPrimaryKey(ScenicSpotGroup record);
}