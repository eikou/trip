package com.voyager.trip.scenic.dao;

import com.voyager.trip.scenic.entity.ScenicMap;

public interface ScenicMapMapper {
    int deleteByPrimaryKey(String id);

    int insert(ScenicMap record);

    int insertSelective(ScenicMap record);

    ScenicMap selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ScenicMap record);

    int updateByPrimaryKey(ScenicMap record);
}