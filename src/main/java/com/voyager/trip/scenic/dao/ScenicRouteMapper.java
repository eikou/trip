package com.voyager.trip.scenic.dao;

import com.voyager.trip.scenic.entity.ScenicRoute;

public interface ScenicRouteMapper {
    int deleteByPrimaryKey(String id);

    int insert(ScenicRoute record);

    int insertSelective(ScenicRoute record);

    ScenicRoute selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ScenicRoute record);

    int updateByPrimaryKey(ScenicRoute record);
}