package com.voyager.trip.scenic.dao;

import com.voyager.trip.scenic.entity.ScenicRouteDetail;

public interface ScenicRouteDetailMapper {
    int deleteByPrimaryKey(String id);

    int insert(ScenicRouteDetail record);

    int insertSelective(ScenicRouteDetail record);

    ScenicRouteDetail selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ScenicRouteDetail record);

    int updateByPrimaryKey(ScenicRouteDetail record);
}