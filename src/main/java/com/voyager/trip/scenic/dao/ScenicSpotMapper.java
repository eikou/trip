package com.voyager.trip.scenic.dao;

import com.voyager.trip.scenic.entity.ScenicSpot;

public interface ScenicSpotMapper {
    int deleteByPrimaryKey(String id);

    int insert(ScenicSpot record);

    int insertSelective(ScenicSpot record);

    ScenicSpot selectByPrimaryKey(String id);

    int updateByPrimaryKeySelective(ScenicSpot record);

    int updateByPrimaryKeyWithBLOBs(ScenicSpot record);

    int updateByPrimaryKey(ScenicSpot record);
    
}