package com.voyager.trip.scenic.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.voyager.trip.scenic.dao.ScenicSpotMapper;
import com.voyager.trip.scenic.dao.ScenicSpotWithMapMapper;
import com.voyager.trip.scenic.service.ScenicSpotService;
import com.voyager.trip.scenic.vo.ScenicSpotVO;

@Service
public class ScenicSpotServiceImpl implements ScenicSpotService {

	@Autowired
	ScenicSpotMapper scenicSpotMapper;
	
	@Autowired
	ScenicSpotWithMapMapper scenicSpotWithMapMapper;

	@Override
	public ScenicSpotVO getById(String id) {
		return scenicSpotWithMapMapper.selectScenicSpotAndMapById(id);
	}

	@Override
	public List<ScenicSpotVO> getScenicListByGroupType(Integer groupType) {
		if (groupType == null) {
			return scenicSpotWithMapMapper.selectScenicAllList();
		} else {
			return scenicSpotWithMapMapper.selectScenicListByGroupType(groupType);
		}
	}
	
	
	
}
