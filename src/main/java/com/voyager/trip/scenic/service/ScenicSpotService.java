package com.voyager.trip.scenic.service;

import java.util.List;

import com.voyager.trip.scenic.vo.ScenicSpotVO;

public interface ScenicSpotService {

	ScenicSpotVO getById(String id);
	
	/**
	 * 按分组类型查询景点列表
	 * @param groupType
	 * @return
	 */
	List<ScenicSpotVO> getScenicListByGroupType(Integer groupType);
	
}
