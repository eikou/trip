package com.voyager.trip.scenic.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.voyager.trip.common.result.JsonResult;
import com.voyager.trip.scenic.service.ScenicSpotService;

@RestController
@RequestMapping("/api/scenicSpot")
public class ScenicApiController {

	@Autowired
	private ScenicSpotService scenicSpotService;
	
	@GetMapping("/{id}")
	public JsonResult getScenicSpotById(@PathVariable String id) {
		return JsonResult.ok().put("scenicSpot", scenicSpotService.getById(id));
	}
	
	@GetMapping("/list")
	public JsonResult getScenicSpotsByGroupType(Integer groupType) {
		return JsonResult.ok().put("list", scenicSpotService.getScenicListByGroupType(groupType));
	}
	
}
